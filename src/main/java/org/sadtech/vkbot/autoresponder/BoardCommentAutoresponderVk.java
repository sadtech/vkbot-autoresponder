package org.sadtech.vkbot.autoresponder;

import org.sadtech.autoresponder.entity.Unit;
import org.sadtech.bot.autoresponder.GeneralAutoresponder;
import org.sadtech.bot.core.domain.content.BoardComment;
import org.sadtech.bot.core.service.ContentService;
import org.sadtech.bot.core.service.sender.Sent;

import java.util.Set;

public class BoardCommentAutoresponderVk extends GeneralAutoresponder<BoardComment> {
    public BoardCommentAutoresponderVk(Set<Unit> menuUnit, Sent sent, ContentService<BoardComment> eventService) {
        super(menuUnit, sent, eventService);
    }
}
