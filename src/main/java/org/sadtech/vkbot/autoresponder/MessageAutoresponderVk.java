package org.sadtech.vkbot.autoresponder;

import org.sadtech.autoresponder.entity.Unit;
import org.sadtech.bot.autoresponder.GeneralAutoresponder;
import org.sadtech.bot.core.domain.content.Mail;
import org.sadtech.bot.core.service.ContentService;
import org.sadtech.bot.core.service.sender.Sent;

import java.util.Set;

public class MessageAutoresponderVk extends GeneralAutoresponder<Mail> {
    public MessageAutoresponderVk(Set<Unit> menuUnit, Sent sent, ContentService<Mail> eventService) {
        super(menuUnit, sent, eventService);
    }
}
